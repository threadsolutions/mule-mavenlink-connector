package org.mule.modules.automation.testcases;

import java.io.IOException;
import java.util.List;
import java.util.Map;


import com.sun.jersey.api.client.ClientResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mule.modules.mavenkink.client.MavenlinkClient;
import org.mule.modules.mavenlink.MavenlinkConnector;
import org.mule.modules.mavenlink.exception.AuthenticationException;
import org.mule.modules.mavenlink.exception.InvalidParameterException;
import org.mule.modules.mavenlink.exception.MavenlinkRestConnectorException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;

public class MavenlinkConnectorTestCases
{
    private static final int ZERO = 0;

	private static final int anId = 1;

	private MavenlinkConnector connector;
	
	@Mock private ClientResponse clientResponse = Mockito.mock(ClientResponse.class);
	@Mock private MavenlinkClient mavenlinkClient = Mockito.mock(MavenlinkClient.class);
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Before
	public void setup() {
        connector = getConnector();
    }
    
    @Test
    public void when_datePerformed_not_valid_throws_InvalidParameterException() throws ClientHandlerException, UniformInterfaceException, Exception{
    	expectedException(InvalidParameterException.class, "datePerformed must be of format ISO-8601 yyyy/mm/dd");
    	connector.insertTimeEntry(1, "", 1, true, "any", null, null, null);
    }
    
    @Test
    public void when_story_id_less_or_equal_than_zero_throws_InvalidParameterException() throws ClientHandlerException, UniformInterfaceException, Exception{
    	expectedException(InvalidParameterException.class, "storyId if present must be greater than 0");
    	connector.insertTimeEntry(anId, "2015/03/08", 1, true, "any", null, 0, null);
    }
    
    @Test
    public void when_workspace_id_less_or_equal_than_zero_throws_InvalidParameterException() throws ClientHandlerException, UniformInterfaceException, Exception{
    	expectedException(InvalidParameterException.class, "workspaceId must be greater than 0");
    	connector.insertTimeEntry(ZERO, "2015/03/08", 1, true, "any", null, null, null);
    }
    
    @Test
    public void when_user_id_less_or_equal_than_zero_throws_InvalidParameterException() throws ClientHandlerException, UniformInterfaceException, Exception{
    	expectedException(InvalidParameterException.class, "userId if present must be greater than 0");
    	connector.insertTimeEntry(anId, "2015/03/08", 1, true, "any", null, null, 0);
    }
    
    @Test
    public void when_time_in_minutes_less_than_zero_throws_InvalidParameterException() throws ClientHandlerException, UniformInterfaceException, Exception{
    	expectedException(InvalidParameterException.class, "timeInMinutes cannot be negative");
    	connector.insertTimeEntry(anId, "2015/03/08", -1, true, "any", null, null, 1);
    }
    
	@Test
    public void connector_insert_time_entry_calls_client_with_all_parameters() throws ClientHandlerException, UniformInterfaceException, Exception{
    	connector.setClient(mavenlinkClient);
    	connector.insertTimeEntry(anId, "2015/03/08", 10, true, "any", 33, 11, 1);
    	verify(mavenlinkClient).insertTimeEntry(anId, "2015/03/08", 10, true, "any", 33, 11, 1);
    }
    
    @Test
    public void connector_get_entries_calls_client_with_the_additional_parameters() throws ClientHandlerException, UniformInterfaceException, Exception{
    	connector.setClient(mavenlinkClient);
    	connector.getTimeEntries(200, 3, true);
    	verify(mavenlinkClient).getTimeEntries(buildAdditionalParameters(200, 3, true));
    }
    
    @Test
    public void connector_destroy_entry_calls_client_with_entry_id() throws ClientHandlerException, UniformInterfaceException, Exception{
    	connector.setClient(mavenlinkClient);
    	connector.destroyTimeEntry(1);
    	verify(mavenlinkClient).destroyTimeEntry(1);
    }
    
    @Test
    public void process_delete_returns_json_ok_when_status_204() throws ClientHandlerException, UniformInterfaceException, Exception{
    	when(clientResponse.getStatus()).thenReturn(204);
    	MavenlinkClient client = new MavenlinkClient(new MavenlinkConnector());
    	Map<String, String> result = client.processDeleteResponse(clientResponse);
    	assertEquals(jsonOK(), result);
    }
    
    @Test
    public void process_delete_returns_json_error_when_status_not_204() throws ClientHandlerException, UniformInterfaceException, Exception{
    	when(clientResponse.getStatus()).thenReturn(404);
    	when(clientResponse.getEntity(String.class)).thenReturn("anyMessage");
    	MavenlinkClient client = new MavenlinkClient(new MavenlinkConnector());
    	Map<String, String> result = client.processDeleteResponse(clientResponse);
    	assertEquals(jsonKO(), result);
    }
    
    @Test
    public void extract_API_response_as_map() throws JsonParseException, JsonMappingException, IOException, AuthenticationException, MavenlinkRestConnectorException{
    	when(clientResponse.getEntity(Map.class)).thenReturn(buildResponseMap());
    	when(clientResponse.getStatus()).thenReturn(200);
    	MavenlinkClient client = new MavenlinkClient(new MavenlinkConnector());
    	List<Map<String, String>> processResponse = client.processResponse("time_entries", clientResponse);
    	assertEquals(processResponse.get(0).get("notes"), "testABU");
    }
    
	@SuppressWarnings("unchecked")
	private Map<String, String> buildResponseMap() throws IOException, JsonParseException, JsonMappingException {
		String sampleResp = "{" + "\"count\": \"2\"," + "\"time_entries\": {" + "  \"230911367\": {"
				+ "    \"created_at\": \"2016-08-12T07:13:13-07:00\","
				+ "    \"updated_at\": \"2016-08-12T07:13:13-07:00\"," + "    \"date_performed\": \"2016-12-08\","
				+ "    \"time_in_minutes\": \"120\"," + "    \"billable\": \"true\"," + "    \"notes\": \"testABU\","
				+ "    \"rate_in_cents\": \"5000\"," + "    \"currency\": \"EUR\"," + "    \"currency_symbol\": \"€\","
				+ "    \"currency_base_unit\": \"100\"," + "    \"user_can_edit\": \"true\","
				+ "    \"approved\": \"false\"," + "    \"taxable\": \"null\"," + "    \"story_id\": \"null\","
				+ "    \"workspace_id\": \"10085667\"," + "    \"user_id\": \"5719327\"," + "    \"id\": \"230911367\""
				+ "  }," + "  \"230912037\": {" + "    \"created_at\": \"2016-08-12T07:14:26-07:00\","
				+ "    \"updated_at\": \"2016-08-12T07:14:26-07:00\"," + "    \"date_performed\": \"2016-12-08\","
				+ "    \"time_in_minutes\": \"120\"," + "    \"billable\": \"true\"," + "    \"notes\": \"testABU\","
				+ "    \"rate_in_cents\": \"5000\"," + "    \"currency\": \"EUR\"," + "    \"currency_symbol\": \"€\","
				+ "    \"currency_base_unit\": \"100\"," + "    \"user_can_edit\": \"true\","
				+ "    \"approved\": \"false\"," + "    \"taxable\": \"null\"," + "    \"story_id\": \"null\","
				+ "    \"workspace_id\": \"10085667\"," + "    \"user_id\": \"5719327\"," + "    \"id\": \"230912037\""
				+ "  }" + "}," + "\"results\": [" + "  {" + "    \"key\": \"time_entries\","
				+ "    \"id\": \"230912037\"" + "  }," + "  {" + "    \"key\": \"time_entries\","
				+ "    \"id\": \"230911367\"" + "  }" + "]" + "}";
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(sampleResp, Map.class);
	}

	private MavenlinkConnector getConnector() {
		return new MavenlinkConnector();
	}
	
	private void expectedException(Class<? extends Exception> exceptionClass, String message) {
		thrown.expect(exceptionClass);
    	thrown.expectMessage(message);
	}
	
	private Map<String, Object> buildAdditionalParameters(Integer perPage, Integer workspaceId, boolean fromArchivedWorkspaces) {
		Map<String, Object> additionalParameters = ImmutableMap.<String, Object>builder().
				put("workspace_id", workspaceId != null ? workspaceId : "").
				put("per_page", perPage).
				put("from_archived_workspaces", fromArchivedWorkspaces).
	    	    build();
		return additionalParameters;
	}
	
	private Map<String, Object> jsonOK() {
		Map<String, Object> additionalParameters = ImmutableMap.<String, Object>builder()
				.put("result", "success")
				.put("message", "")
	    	    .build();
		return additionalParameters;
	}
	
	private Map<String, Object> jsonKO() {
		Map<String, Object> additionalParameters = ImmutableMap.<String, Object>builder()
				.put("result", "error")
				.put("message", "anyMessage")
	    	    .build();
		return additionalParameters;
	}
}
