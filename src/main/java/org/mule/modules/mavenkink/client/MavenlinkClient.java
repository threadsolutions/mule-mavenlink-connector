package org.mule.modules.mavenkink.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.mule.modules.mavenlink.MavenlinkConnector;
import org.mule.modules.mavenlink.exception.AuthenticationException;
import org.mule.modules.mavenlink.exception.MavenlinkRestConnectorException;

import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class MavenlinkClient {
	
	private Client client;
	private MavenlinkConnector connector;
	private WebResource apiResource;

	public MavenlinkClient(MavenlinkConnector connector) {
		setConnector(connector);
		this.client = Client.create(buildClient());
		this.apiResource = this.client.resource(getConnector().getApiUrl() + "/" + getConnector().getApiVersion());
	}

	public WebResource getApiResource() {
		return apiResource;
	}
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public void setApiResource(WebResource apiResource) {
		this.apiResource = apiResource;
	}

	public MavenlinkConnector getConnector() {
		return connector;
	}

	public void setConnector(MavenlinkConnector connector) {
		this.connector = connector;
	}

	public List<Map<String,String>> getTimeEntries(Map<String, Object> additionalParameters) throws ClientHandlerException, UniformInterfaceException, Exception {
		
		FormDataBuilder formDataBuilder = new FormDataBuilder();
		additionalParameters.forEach((key, value) -> formDataBuilder.with(key, value));

		return executeGET(buildWebResource(formDataBuilder.build()), "time_entries");
	}

	public Map<String, String> insertTimeEntry(Integer workspaceId, String datePerformed, Integer timeInMinutes, Boolean billable,
			String notes, Integer rateInCents, Integer storyId, Integer userId)
			throws ClientHandlerException, UniformInterfaceException, AuthenticationException, MavenlinkRestConnectorException {
		
		MultivaluedMap<String, String> formData = new FormDataBuilder()
			.with("time_entry[workspace_id]", workspaceId)
			.with("time_entry[date_performed]", datePerformed)
			.with("time_entry[time_in_minutes]", timeInMinutes)
			.with("time_entry[billable]", billable)
			.with("time_entry[notes]", notes)
			.with("time_entry[rate_in_cents]", rateInCents)
			.with("time_entry[story_id]", storyId)
			.with("time_entry[user_id]", userId)
			.build();

		return executePOST(buildWebResource(formData), "time_entries");
	}

	public Map<String, String> destroyTimeEntry(Integer id) {
		
		ClientResponse clientResponse = getApiResource().path("time_entries/" + id + ".json")
		.accept(MediaType.APPLICATION_JSON)
		.type(MediaType.APPLICATION_JSON)
		.header("Authorization", "Bearer " + getConnector().getAccessToken())
		.delete(ClientResponse.class);
		
		return processDeleteResponse(clientResponse);
	}

	public Map<String, String> processDeleteResponse(ClientResponse clientResponse) {
		if (clientResponse.getStatus() == 204) {
			return jsonOK();
		}
		return jsonKO(clientResponse.getEntity(String.class));
	}

	private Map<String, String> executePOST(Builder builder, String resultKey) throws AuthenticationException, MavenlinkRestConnectorException {
		
		ClientResponse clientResponse = builder.post(ClientResponse.class);
		List<Map<String, String>> result = processResponse(resultKey, clientResponse);
		
		if(result.size() > 0)
			return result.get(0);
		return new HashMap<String, String>();
	}
	
	private List<Map<String,String>> executeGET(Builder builder, String resultKey) throws ClientHandlerException, UniformInterfaceException, Exception {
		
		ClientResponse clientResponse = builder.method("GET", ClientResponse.class);
		return processResponse(resultKey, clientResponse);
	}

	public List<Map<String, String>> processResponse(String resultKey, ClientResponse clientResponse) throws AuthenticationException, MavenlinkRestConnectorException {
		if (clientResponse.getStatus() == 200) {
			Map<?,?> entity = clientResponse.getEntity(Map.class);
			@SuppressWarnings("unchecked")
			Map<String, Map<String,String>> content = (Map<String, Map<String,String>>) entity.get(resultKey);

			List<Map<String,String>> list = new ArrayList<>(content.values());

			return list;
		} else if (clientResponse.getStatus() == 401) {
			throw new AuthenticationException("The access token has expired; " + clientResponse.getEntity(String.class));
		} else {
			throw new MavenlinkRestConnectorException(String.format("ERROR - statusCode: %d - message: %s", clientResponse.getStatus(),
					clientResponse.getEntity(String.class)));
		}
	}
	
	private ClientConfig buildClient() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		return clientConfig;
	}

	private Builder buildWebResource(MultivaluedMap<String, String> formData) {
		return getApiResource().path("time_entries.json")
				.queryParams(formData).accept(MediaType.APPLICATION_JSON)
				.type(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + getConnector().getAccessToken());
	}
	
	private Map<String, String> jsonOK() {
		Map<String, String> additionalParameters = ImmutableMap.<String, String>builder()
				.put("result", "success")
				.put("message", "")
	    	    .build();
		return additionalParameters;
	}
	
	private Map<String, String> jsonKO(String string) {
		Map<String, String> additionalParameters = ImmutableMap.<String, String>builder()
				.put("result", "error")
				.put("message", string)
	    	    .build();
		return additionalParameters;
	}

}
