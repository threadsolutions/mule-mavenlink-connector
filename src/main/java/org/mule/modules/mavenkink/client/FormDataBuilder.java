package org.mule.modules.mavenkink.client;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.core.util.MultivaluedMapImpl;

public class FormDataBuilder {
	
	private MultivaluedMap<String, String> formData;
	
	public FormDataBuilder(){
		 this.formData = new MultivaluedMapImpl();
	}
	
	public FormDataBuilder with(String parameter, Object value) {
		if(value != null)
			this.formData.add(parameter, String.valueOf(value));
		return this;
	}

	public MultivaluedMap<String, String> build() {
		return this.formData;		
	}
	
}
