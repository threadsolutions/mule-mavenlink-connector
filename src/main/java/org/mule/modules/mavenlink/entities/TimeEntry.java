package org.mule.modules.mavenlink.entities;

import java.util.Date;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({ "created_at", "updated_at", "date_performed", "time_in_minutes", "billable", "notes", "rete_in_cents", "currency", "currency_symbol"
	, "currency_base_unit", "user_can_edit", "approved", "taxable", "story_id", "workaspace_id", "user_id", "id"})
public class TimeEntry {
	
	@JsonProperty("created_at")
	private Date createdAt;
	
	@JsonProperty("updated_at")
	private Date updatedAt;
	
	@JsonProperty("date_performed")
	private Date datePerformed;
	
	@JsonProperty("time_in_minutes")
	private Long timeInMinutes;
	
	@JsonProperty("billable")
	private boolean billable;
	
	@JsonProperty("notes")
	private String notes;
	
	@JsonProperty("rete_in_cents")
	private Long rateInCents;
	
	@JsonProperty("currency")
	private String currency;
	
	@JsonProperty("currency_symbol")
	private String currencySymbol;
	
	@JsonProperty("currency_base_unit")
	private String currencyBaseUnit;
	
	@JsonProperty("user_can_edit")
	private boolean userCanEdit;
	
	@JsonProperty("approved")
	private boolean approved;
	
	@JsonProperty("taxable")
	private boolean taxable;
	
	@JsonProperty("story_id")
	private Long storyId;
	
	@JsonProperty("workspace_id")
	private Long workspaceId;
	
	@JsonProperty("user_id")
	private Long userId;
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}
	
	@JsonProperty("created_at")
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	@JsonProperty("updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@JsonProperty("updated_at")
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@JsonProperty("date_performed")
	public Date getDatePerformed() {
		return datePerformed;
	}
	
	@JsonProperty("date_performed")
	public void setDatePerformed(Date datePerformed) {
		this.datePerformed = datePerformed;
	}
	
	@JsonProperty("time_in_minutes")
	public Long getTimeInMinutes() {
		return timeInMinutes;
	}
	
	@JsonProperty("time_in_minutes")
	public void setTimeInMinutes(Long timeInMinutes) {
		this.timeInMinutes = timeInMinutes;
	}
	
	@JsonProperty("billable")
	public boolean isBillable() {
		return billable;
	}
	
	@JsonProperty("billable")
	public void setBillable(boolean billable) {
		this.billable = billable;
	}
	
	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}
	
	@JsonProperty("notes")
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@JsonProperty("rete_in_cents")
	public Long getRateInCents() {
		return rateInCents;
	}
	
	@JsonProperty("rete_in_cents")
	public void setRateInCents(Long rateInCents) {
		this.rateInCents = rateInCents;
	}
	
	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}
	
	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@JsonProperty("currency_symbol")
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	
	@JsonProperty("currency_symbol")
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	
	@JsonProperty("currency_base_unit")
	public String getCurrencyBaseUnit() {
		return currencyBaseUnit;
	}
	
	@JsonProperty("currency_base_unit")
	public void setCurrencyBaseUnit(String currencyBaseUnit) {
		this.currencyBaseUnit = currencyBaseUnit;
	}
	
	@JsonProperty("user_can_edit")
	public boolean isUserCanEdit() {
		return userCanEdit;
	}
	
	@JsonProperty("user_can_edit")
	public void setUserCanEdit(boolean userCanEdit) {
		this.userCanEdit = userCanEdit;
	}
	
	@JsonProperty("approved")
	public boolean isApproved() {
		return approved;
	}
	
	@JsonProperty("approved")
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	
	@JsonProperty("taxable")
	public boolean isTaxable() {
		return taxable;
	}
	
	@JsonProperty("taxable")
	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}
	
	@JsonProperty("story_id")
	public Long getStoryId() {
		return storyId;
	}
	
	@JsonProperty("story_id")
	public void setStoryId(Long storyId) {
		this.storyId = storyId;
	}
	
	@JsonProperty("workspace_id")
	public Long getWorkspaceId() {
		return workspaceId;
	}
	
	@JsonProperty("workspace_id")
	public void setWorkspaceId(Long workspaceId) {
		this.workspaceId = workspaceId;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}
	
	@JsonProperty("user_id")
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}
}
