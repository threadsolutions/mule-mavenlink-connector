package org.mule.modules.mavenlink.exception;

public class AuthenticationException extends Exception{

	public AuthenticationException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
