package org.mule.modules.mavenlink.exception;

public class MavenlinkRestConnectorException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3034397690086143109L;

	public MavenlinkRestConnectorException(String string) {
		super(string);
	}

}
