package org.mule.modules.mavenlink.exception;

public class InvalidParameterException extends Exception{

	public InvalidParameterException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
