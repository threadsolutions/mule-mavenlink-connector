package org.mule.modules.mavenlink;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;
import org.mule.api.annotations.lifecycle.Start;
import org.mule.api.annotations.oauth.OAuthAccessToken;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.Optional;
import org.mule.modules.mavenkink.client.MavenlinkClient;
import org.mule.modules.mavenlink.exception.InvalidParameterException;

import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;

@Connector(name="mavenlink", friendlyName="Mavenlink")
public class MavenlinkConnector {

	private MavenlinkClient client;
	
	private static final String DATE_FORMAT = "mm/dd/yyyy";
	
	@Configurable @Default("https://api.mavenlink.com/api")
	private String apiUrl;
	
	@Configurable @Default("v1")
	private String apiVersion;
	
	@Configurable
	@OAuthAccessToken
	private String accessToken;
	
	public MavenlinkClient getClient() {
		return client;
	}

	public void setClient(MavenlinkClient client) {
		this.client = client;
	}
	
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Start
	public void init() {
		this.client = new MavenlinkClient(this);
	}
	
	@Processor
	public List<Map<String, String>> getTimeEntries(@Default("20") Integer perPage, @Optional Integer workspaceId, @Default("false") boolean fromArchivedWorkspaces) throws ClientHandlerException, UniformInterfaceException, Exception {
	    return getClient().getTimeEntries(buildAdditionalParameters(perPage, workspaceId, fromArchivedWorkspaces));
	}

	@Processor
	public Map<String, String> insertTimeEntry(Integer workspaceId, String datePerformed, Integer timeInMinutes, @Optional Boolean billable, @Optional String notes, @Optional Integer rateInCents,
			@Optional Integer storyId, @Optional Integer userId) throws ClientHandlerException, UniformInterfaceException, Exception {
	    
		if(!isParsable(datePerformed))
			throw buildInvalidParameterException("datePerformed must be of format ISO-8601 yyyy/mm/dd");
		
		if(lessThanZero(storyId))
			throw buildInvalidParameterException("storyId if present must be greater than 0");
		
		if(lessThanZero(workspaceId))
			throw buildInvalidParameterException("workspaceId must be greater than 0");
		
		if(lessThanZero(userId))
			throw buildInvalidParameterException("userId if present must be greater than 0");
		
		if(lessThanZero(timeInMinutes))
			throw buildInvalidParameterException("timeInMinutes cannot be negative");
		
		return getClient().insertTimeEntry(workspaceId, datePerformed, timeInMinutes, billable, notes, rateInCents, storyId, userId);
	}
	
	@Processor
	public Object destroyTimeEntry(Integer id) {
		return getClient().destroyTimeEntry(id);
	}
	
	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	
	private boolean isParsable(String datePerformed) {
		try {
			if(datePerformed != null) {
				new SimpleDateFormat(DATE_FORMAT).parse(datePerformed);
				return true;
			}
		} catch (ParseException e) {
			return false;
		}
		return false;
	}
	
	private boolean lessThanZero(Integer storyId) {
		return storyId != null && storyId <= 0;
	}
	
	private InvalidParameterException buildInvalidParameterException(String message) {
		return new InvalidParameterException(message);
	}
	
	private Map<String, Object> buildAdditionalParameters(Integer perPage, Integer workspaceId, boolean fromArchivedWorkspaces) {
		Map<String, Object> additionalParameters = ImmutableMap.<String, Object>builder().
				put("workspace_id", workspaceId != null ? workspaceId : "").
				put("per_page", perPage).
				put("from_archived_workspaces", fromArchivedWorkspaces).
	    	    build();
		return additionalParameters;
	}

}